﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Animator animator;
    public SpriteRenderer spriteRendererE;
    Rigidbody2D rbE2D;

    public Vector2 moveTo;
    public float speed = 1f;
    public float progress;
    Vector2 beginPos;
    Vector2 endPos;



    void Start()
    {
        GameController.gc.enemyList.Add(this);
        rbE2D = GetComponent<Rigidbody2D>();
        beginPos = transform.position;
        endPos = beginPos + moveTo;
    }

    void Update()
    {
        float progress = Mathf.PingPong(Time.time * speed, 1f);

        transform.position = Vector2.Lerp(beginPos, endPos, progress);

        if (rbE2D.position.x == endPos.x)
        { 
            spriteRendererE.flipX = false;
        }

        if (rbE2D.position.x == beginPos.x)
        {
            spriteRendererE.flipX = true;
        }
    }


    private void FixedUpdate()
    {

    }
 

    private void OnDestory()
    {
        GameController.gc.enemyList.Remove(this);
    }
    
}
