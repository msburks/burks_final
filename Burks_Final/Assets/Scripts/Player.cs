﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour
{

    Rigidbody2D rb2D;
    public SpriteRenderer spriteRenderer;
    public SpriteRenderer spriteRenderer2;
    public Animator animator1;
    public Animator animator2;
    public AudioSource backgroundMusic;
    public AudioSource jumpSound;

    public float runSpeed;
    public float jumpForce;
    bool isDead;
    public GameObject headSprite;
    public GameObject bodySprite;
    public GameObject headHUD;
    public GameObject bodyHUD;

    //All UI Stuff
    public TextMeshProUGUI deathCounter_text;
    public TextMeshProUGUI counter_text;
    float counter;
    private float timer = 120;
    public float restartTimer;
    public TextMeshProUGUI timer_text;
    public GameObject death_text;
    public GameObject begin_text;
    public GameObject win_text;
    bool begin;
    bool win;


    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        GameController.gc.player = this;
        death_text.SetActive(false);
        begin = true;
        win = false;

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");


            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }

        }
        if (begin == true)
        {
            begin_text.SetActive(true);
            Pause();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Resume();
                begin_text.SetActive(false);
                begin = false;
            }
        }
        if (isDead == true)
        {
            death_text.SetActive(true);
            Pause();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Resume();
                EndLevel();
            }


        }
        if (win == true)
        {
            win_text.SetActive(true);
            Pause();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Resume();
                EndLevel();
            }


        }
        SetCountText();
        SetTimerText();
    }

    private void FixedUpdate()
    {
        //allows the player to move
        float horizontalInput = Input.GetAxis("Horizontal");

        rb2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rb2D.velocity.y);
        //flips the sprite
        if (rb2D.velocity.x > 0)
            spriteRenderer.flipX = false;

        else
        if (rb2D.velocity.x < 0)
        { spriteRenderer.flipX = true; }

        if (bodySprite == true)
        {
            if (Mathf.Abs(horizontalInput) > 0f)
                animator2.SetBool("isRunning", true);
            else
                animator2.SetBool("isRunning", false);
        }
        
    }

    void Jump()
    {
        rb2D.velocity = new Vector2(rb2D.velocity.x, jumpForce);
        jumpSound.Play();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("PickUp")) //compares tag to see if matches str
        {
            collision.gameObject.SetActive(false);
            GameController.gc.score += 1;

        }
        if (collision.gameObject.CompareTag("PickUp2")) //compares tag to see if matches str
        {
            collision.gameObject.SetActive(false);
            GameController.gc.score += 2;

        }

        if (collision.gameObject.CompareTag("Switch")) //compares tag to see if matches str
        {
            collision.gameObject.SetActive(false);
            spriteRenderer = spriteRenderer2;
            headSprite.SetActive(false);
            bodySprite.SetActive(true);
            headHUD.SetActive(false);
            bodyHUD.SetActive(true);


        }
        if (collision.gameObject.CompareTag("Death")) //compares tag to see if matches str
        {
            //PauseGame();
            //Invoke("ResumeGame", restartTimer);
            //Invoke("EndLevel", restartTimer);
            //EndLevel();
            GameController.gc.deaths += 1;
            isDead = true;

        }
        if (collision.gameObject.CompareTag("win")) //compares tag to see if matches str
        {
            win = true;

        }
    }
    void SetCountText()
    {

        counter_text.text = "Money: " + GameController.gc.score;
        deathCounter_text.text = "" + GameController.gc.deaths;

    }
    void SetTimerText()
    {
        timer -= Time.deltaTime;
        timer_text.text = "Timer Left: " + (int)timer;

    }

    void EndLevel()
    {
        SceneManager.LoadScene("Level_1");
    }


    private void Pause()
    {
        Time.timeScale = 0f;
    }
    private void Resume()
    {
        Time.timeScale = 1f;

    }
}