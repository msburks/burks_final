﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointMovement : MonoBehaviour
{
    public SpriteRenderer spriteRendererE;
    Rigidbody2D rbE2D;

    public Vector2 moveTo;
    public float speed = 1f;

    Vector2 beginPos;
    Vector2 endPos;



    void Start()
    {
        rbE2D = GetComponent<Rigidbody2D>();
        beginPos = transform.position;
        endPos = beginPos + moveTo;
    }

    void Update()
    {
        float progress = Mathf.PingPong(Time.time * speed, 1f);

        transform.position = Vector2.Lerp(beginPos, endPos, progress);
    }

    private void FixedUpdate()
    {
        if (rbE2D.velocity.x > 0)
        {
            spriteRendererE.flipX = false;
        }
        else
        if (rbE2D.velocity.x < 0)
        {
            spriteRendererE.flipX = true;
        }
    }




    /*
    public Transform[] waypoints;   //array to hold all the waypoints in the sprite's path

    [SerializeField]
    public float moveSpeed = 2.0f;

    public int wayPointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = waypoints[wayPointIndex].transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    public void Move()
    {
        if (wayPointIndex == waypoints.Length)
        {
            wayPointIndex = 0;
        }
        if (wayPointIndex <= waypoints.Length - 1)
        {
            transform.position = Vector3.MoveTowards(transform.position, waypoints[wayPointIndex].transform.position, moveSpeed * Time.deltaTime);

            if (transform.position == waypoints[wayPointIndex].transform.position)
            {
                wayPointIndex += 1;
            }
        }
        
        
    }
    */




}
